﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBooks
{
    class OnlineStore
    {
        private List<Book> orders;
        
        public OnlineStore()
        {
            orders = new List<Book>();
        } 


        public void AddOrder(Book order)
        {
            orders.Add(order);
        }

        public void RemoveOrder(int position)
        {
            int index = position - 1;

            orders.RemoveAt(index);
        }

        public void ClearOrders()
        {
            orders.Clear();
        }

        public void PrintAllOrders()
        {
            if (orders.Count == 0)
            {
                Console.WriteLine("Список заказов пуст!");
            }
            else
            {
                Console.WriteLine();

                for (int i = 0; i < orders.Count; i++)
                {
                    Console.WriteLine((i + 1) + ". " + orders[i].PrintBook());
                }

                Console.WriteLine();
            }
        }


        public int OrdersCount
        {
            get { return orders.Count; }
        }

    }
}
