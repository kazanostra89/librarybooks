﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LibraryBooks
{
    class WorkingWithTheLibrary
    {
        private Library library;
        private OnlineStore onlineStore;

        public WorkingWithTheLibrary()
        {
            library = new Library(AssistantClass.InputString("Введите название библиотеки: "));

            onlineStore = new OnlineStore();
        }

        private void PrintMenuOnlineStore()
        {
            Console.WriteLine("Вас приветствует Online Store: Rabbit\n");
            Console.WriteLine("1. Добавить заказ");
            Console.WriteLine("2. Удалить заказ");
            Console.WriteLine("3. Показать ваш список заказов");
            Console.WriteLine("4. Очистить список заказов");
            Console.WriteLine("5. Показать список доступных к покупке товаров");
            Console.WriteLine("0. Вернуться в библиотеку\n");
        }

        private void StartOnlineStore()
        {
            bool startMenuStore = true;
            int selectMenuStore;

            while (startMenuStore)
            {
                Console.Clear();
                PrintMenuOnlineStore();

                selectMenuStore = AssistantClass.InputInt("Выберите пунк меню: ");

                switch (selectMenuStore)
                {
                    case 1:
                        if (library.ListCount == 0)
                        {
                            Console.WriteLine("Список доступных для покупки книг пуст!");
                        }
                        else
                        {
                            library.PrintAllBooks();

                            int positionOrder;

                            do
                            {
                                positionOrder = AssistantClass.InputInt("Введите доступную позицию товара: ");

                            } while (positionOrder < 1 || positionOrder > library.ListCount);

                            onlineStore.AddOrder(library.ExecBookToOrder(positionOrder));

                            Console.WriteLine("Товар: \n" + library.ExecBookToOrder(positionOrder).PrintBook()
                                             + "\nдобавлен в ваш список заказов!"
                                             + "\nСпасибо за покупку!");
                        }
                        break;
                    case 2:
                        Console.Clear();

                        if (onlineStore.OrdersCount == 0)
                        {
                            Console.WriteLine("Удаление товара не возможно, список пуст!");
                        }
                        else
                        {
                            onlineStore.PrintAllOrders();

                            int positionDeleteOrder;

                            do
                            {
                                positionDeleteOrder = AssistantClass.InputInt("Введите позицию товара для удаления: ");

                            } while (positionDeleteOrder < 1 || positionDeleteOrder > onlineStore.OrdersCount);

                            onlineStore.RemoveOrder(positionDeleteOrder);

                            Console.WriteLine();
                            onlineStore.PrintAllOrders();
                        }
                        break;
                    case 3:
                        onlineStore.PrintAllOrders();
                        break;
                    case 4:
                        onlineStore.ClearOrders();
                        onlineStore.PrintAllOrders();
                        break;
                    case 5:
                        library.PrintAllBooks();
                        break;
                    case 0:
                        startMenuStore = false;
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда");
                        break;
                }

                Console.ReadKey();
            }

        }

        private void PrintMenuLibrary()
        {
            Console.WriteLine("Вас приветствует библиотека: " + library.NameLibrary + "\n");
            Console.WriteLine("1. Добавить книгу в библиотеку");
            Console.WriteLine("2. Удалить книгу из библиотеки");
            Console.WriteLine("3. Показать список всех книг в библиотеке");
            Console.WriteLine("4. Найти книгу по автору");
            Console.WriteLine("5. Найти книгу по издателю");
            Console.WriteLine("6. Найти книги по диапазону дат издания");
            Console.WriteLine("7. Загрузить список книг из файла");
            Console.WriteLine("8. Сохранить список книг в файл");
            Console.WriteLine("9. Очистить список всех книг");
            Console.WriteLine("10. Приобрести данные книги через Online Store: Rabbit");
            Console.WriteLine("0. Завершить работу\n");

        }

        public void StartLibrary()
        {
            bool startMenuLibrary = true;
            int selectMenuLibrary;

            while (startMenuLibrary)
            {
                Console.Clear();
                PrintMenuLibrary();
                selectMenuLibrary = AssistantClass.InputInt("Выберите пунк меню: ");

                switch (selectMenuLibrary)
                {
                    case 1:
                        library.AddBook(new Book(
                            AssistantClass.InputString("Введине название книги: "),
                            AssistantClass.InputString("Введите автора книги: "),
                            AssistantClass.InputString("Введите издателя книги: "),
                            AssistantClass.InputDate("Введите год издания книги: ")
                            ));
                        break;
                    case 2:
                        Console.Clear();

                        if (library.ListCount == 0)
                        {
                            Console.WriteLine("Удаление книги не возможно, список пуст!");
                        }
                        else
                        {
                            library.PrintAllBooks();

                            int position;

                            do
                            {
                                position = AssistantClass.InputInt("Введите позицию книги для удаления: ");

                            } while (position < 1 || position > library.ListCount);

                            library.RemoveBook(position);

                            Console.WriteLine();
                            library.PrintAllBooks();
                        }
                        break;
                    case 3:
                        library.PrintAllBooks();
                        break;
                    case 4:
                        if (library.ListCount == 0)
                        {
                            Console.WriteLine("Список книг пуст, поиск не возможен!");
                        }
                        else
                        {
                            library.PrintAllBooks();
                            library.SearchBooksAuthor(AssistantClass.InputString("Ищем книги по автору: "));
                        }
                        break;
                    case 5:
                        if (library.ListCount == 0)
                        {
                            Console.WriteLine("Список книг пуст, поиск не возможен!");
                        }
                        else
                        {
                            library.PrintAllBooks();
                            library.SearchBooksPublisher(AssistantClass.InputString("Ищем книги по издателю: "));
                        }
                        break;
                    case 6:
                        if (library.ListCount == 0)
                        {
                            Console.WriteLine("Список книг пуст, поиск не возможен!");
                        }
                        else
                        {
                            library.PrintAllBooks();

                            int minDate, maxDate;

                            Console.WriteLine($"Наименьший год публикации издания = {library.GetMinPublicationDate()}, а наибольший = {library.GetMaxPublicationDate()}.");

                            do
                            {
                                minDate = AssistantClass.InputDate("Введите нижнюю границу даты: ");
                                maxDate = AssistantClass.InputDate("Введите верхнюю границу даты: ");

                            } while (minDate < library.GetMinPublicationDate() || minDate > library.GetMaxPublicationDate()
                                    || maxDate < library.GetMinPublicationDate() || maxDate > library.GetMaxPublicationDate() || minDate <= maxDate);

                            library.SearchBooksPublicationDate(minDate, maxDate);
                        }
                        break;
                    case 7:
                        Console.WriteLine("Производится загрузка данных из файла [BooksList.txt]!");
                        library.AddRangeBooks(UploadingFromFile.ExecDataFile());
                        Console.WriteLine("Загрузка данных завершена!");

                        library.PrintAllBooks();
                        break;
                    case 8:
                        library.SavingToFile();
                        break;
                    case 9:
                        library.ClearBooksList();
                        Console.WriteLine("Список очищен!");
                        break;
                    case 10:
                        StartOnlineStore();
                        break;
                    case 0:
                        startMenuLibrary = false;
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда");
                        break;
                }

                Console.ReadKey();
            }



        }


    }
}
