﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBooks
{
    class Book
    {
        private string title;
        private string author;
        private string publisher;
        private int publicationDate;
        
        public Book(string title, string author, string publisher, int publicationDate)
        {
            this.title = title;
            this.author = author;
            this.publisher = publisher;
            this.publicationDate = publicationDate;
        }
        //Этот конструктор используется для ручного добавления книг в коде программы
        public Book(string title, string author, string publisher, int publicationDate, ILibraryNew library)
            : this(title, author, publisher, publicationDate)
        {
            library.AddBook(this);
        }
        

        public string PrintBook()
        {
            return title + "; " + author + "; " + publicationDate + "; " + publisher;
        }


        public string Title
        {
            get { return title; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Publisher
        {
            get { return publisher; }
        }

        public int PublicationDate
        {
            get { return publicationDate; }
        }

    }
}
