﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LibraryBooks
{
    class Library : ILibraryNew
    {
        private string nameLibrary;
        private List<Book> books;

        public Library(string nameLibrary)
        {
            this.nameLibrary = nameLibrary;
            books = new List<Book>();
        }


        public void AddBook(Book book)
        {
            books.Add(book);
        }

        public void AddRangeBooks(List<Book> booksLoad)
        {
            books.AddRange(booksLoad);
        }

        public void RemoveBook(int position)
        {
            int index = position - 1;

            books.RemoveAt(index);
        }

        public void SavingToFile()
        {
            Console.WriteLine("Производится запись в файл [SaveNewList.txt]!");

            StreamWriter writer = new StreamWriter("SaveNewList.txt", false);

            for (int i = 0; i < books.Count; i++)
            {
                writer.WriteLine(books[i].Title);
                writer.WriteLine(books[i].Author);
                writer.WriteLine(books[i].Publisher);
                writer.WriteLine(books[i].PublicationDate);
            }

            writer.Close();
            Console.WriteLine("Запись в файл [SaveNewList.txt] завершена!");
        }

        public void ClearBooksList()
        {
            books.Clear();
        }


        public void PrintAllBooks()
        {
            if (books.Count == 0)
            {
                Console.WriteLine("Список книг пуст!");
            }
            else
            {
                Console.WriteLine();

                for (int i = 0; i < books.Count; i++)
                {
                    Console.WriteLine((i + 1) + ". " + books[i].PrintBook());
                }

                Console.WriteLine();
            }

        }

        public void SearchBooksAuthor(string author)
        {
            foreach (Book book in books.FindAll(item => item.Author == author))
            {
                book.PrintBook();
            }
        }

        public void SearchBooksPublisher(string publisher)
        {
            foreach (Book book in books.FindAll(item => item.Publisher == publisher))
            {
                book.PrintBook();
            }
        }

        public void SearchBooksPublicationDate(int min, int max)
        {
            foreach (Book book in books.FindAll(item => item.PublicationDate >= min && item.PublicationDate <= max))
            {
                book.PrintBook();
            }
        }

        
        public Book ExecBookToOrder(int position)
        {
            int index = position - 1;
            
            return books[index];
        }

        public int GetMinPublicationDate()
        {
            return books.Min(item => item.PublicationDate);
        }

        public int GetMaxPublicationDate()
        {
            return books.Max(item => item.PublicationDate);
        }


        public string NameLibrary
        {
            get { return nameLibrary; }
        }

        public int ListCount
        {
            get { return books.Count; }
        }

    }
}
