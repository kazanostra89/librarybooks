﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LibraryBooks
{
    static class UploadingFromFile
    {
        private static List<Book> booksExec;
        
        public static List<Book> ExecDataFile()
        {
            booksExec = new List<Book>();
            
            StreamReader reader = new StreamReader("BooksList.txt");

            while (!reader.EndOfStream)
            {
                booksExec.Add(new Book(
                    reader.ReadLine(),
                    reader.ReadLine(),
                    reader.ReadLine(),
                    Convert.ToInt32(reader.ReadLine())
                    ));
            }
            
            reader.Close();
            return booksExec;
        }
        
    }
}
