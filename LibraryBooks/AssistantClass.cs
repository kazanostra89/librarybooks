﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBooks
{
    static class AssistantClass
    {
        public static int InputDate(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < 1000 || number > 2050);

            return number;
        }

        public static int InputInt(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        public static string InputString(string message)
        {
            Console.Write(message);

            return Console.ReadLine();
        }
        
    }
}
