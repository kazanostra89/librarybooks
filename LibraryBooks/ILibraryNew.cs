﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBooks
{
    interface ILibraryNew
    {
        void AddBook(Book book);

        void AddRangeBooks(List<Book> booksLoad);

        void RemoveBook(int position);

        void SavingToFile();

        void ClearBooksList();


        void PrintAllBooks();

        void SearchBooksAuthor(string author);

        void SearchBooksPublisher(string publisher);

        void SearchBooksPublicationDate(int min, int max);


        string NameLibrary
        {
            get;
        }

        int ListCount
        {
            get;
        }

    }
}
